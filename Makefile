# TUTO MAKEFILE : https://physino.xyz/learning/2020/02/11/automate-compilation-of-root-based-c++-program-with-makefile/
# https://www.gnu.org/software/make/manual/html_node/Rule-Introduction.html#Rule-Introduction
# AUTO VARIABLES https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html#Automatic-Variables
# PATTERN SPECIFIC VARIABLE : https://www.gnu.org/software/make/manual/html_node/Pattern_002dspecific.html#Pattern_002dspecific

# -----------------------------------------------------------------------------------------------------------------------------
CXX = g++
CXXFLAGS = -Wall $(shell root-config --cflags) # -I /home/user/Bureau/THESE/SIMUG4/root/include/
LDLIBS = $(shell root-config --libs) #-L /home/user/Bureau/THESE/SIMUG4/root/lib/ -lCore -lMathCore -lRIO -lNet -lHist -lGraf -lRIO -lNet -lHist -lGraf -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lpthread -lGraf3d
# -----------------------------------------------------------------------------------------------------------------------------
# SRC = $(wildcard *.cc) # The first line creates a list of all files that end with .cc and save it in $(SRC). 
# EXE = $(SRC:.cc=.exe)  # The second line changes the suffix of every entry in $(SRC) from .cc to .exe and save the new list in $(EXE). 
SRC = $(wildcard *.cc) # list all files that end with .cc
EXE = $(SRC:.cc=)      # remove .cc from those file names
# -----------------------------------------------------------------------------------------------------------------------------
all: $(EXE)
# -----------------------------------------------------------------------------------------------------------------------------
# where $? refers to test.cc and $@ refers to test.exe. 
# test.exe: test.cc
# 	$(CXX) $(CXXFLAGS) $? -o $@ $(LDLIBS)

# write_ROOT_file.exe: write_ROOT_file.cc
# 	$(CXX) $(CXXFLAGS) $? -o $@ $(LDLIBS)

# read_ROOT_file.exe: read_ROOT_file.cc
# 	$(CXX) $(CXXFLAGS) $? -o $@ $(LDLIBS)

# The two rules for test.exe and gaus.exe are very similar. It would be nice if we can combine them in one rule. 
# %.exe: %.cc
# 	@echo "COMPILE $(?)"
# 	$(CXX) $(CXXFLAGS) $? -o $@ $(LDFLAGS) $(LDLIBS)

%: %.cc
	@echo ">> COMPILE $(?)"
	$(CXX) $(CXXFLAGS) $? -o $@ $(LDFLAGS) $(LDLIBS)
	@echo ">> RESULT $@\n-----\n"
# -----------------------------------------------------------------------------------------------------------------------------
clean:
	$(RM) $(EXE)
# -----------------------------------------------------------------------------------------------------------------------------
install:
# -----------------------------------------------------------------------------------------------------------------------------
debug:
	@echo CXXFLAGS = $(CXXFLAGS)
	@echo LDLIBS = $(LDLIBS)
	@echo SRC = $(SRC)
	@echo EXE = $(EXE)

# -----------------------------------------------------------------------------------------------------------------------------
.PHONY: all clean install debug